-- Adminer 4.2.3 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

CREATE DATABASE `seabattle` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `seabattle`;

DROP TABLE IF EXISTS `rooms`;
CREATE TABLE `rooms` (
  `gameid` int(11) NOT NULL AUTO_INCREMENT,
  `user1` char(15) NOT NULL,
  `user2` char(15) NOT NULL,
  `Turn` char(15) NOT NULL DEFAULT '1',
  `user1_ready` int(1) NOT NULL DEFAULT '0',
  `user2_ready` int(1) NOT NULL DEFAULT '0',
  `user1_cells` text NOT NULL,
  `user2_cells` text NOT NULL,
  `winner` char(15) NOT NULL,
  `Player1Started` int(1) NOT NULL DEFAULT '0',
  `Player2Started` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`gameid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` char(15) NOT NULL,
  `password` char(15) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


-- 2016-02-26 10:08:02
